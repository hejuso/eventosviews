package com.example.hejuso.eventosviews;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class EventosViews extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos_views);

        // Puntuación de estrellas
        final TextView ratingText = findViewById(R.id.valor_estrella);
        final RatingBar ratingBar = findViewById(R.id.ratingBar);

        ratingBar.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {

            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating, boolean fromUser){

                ratingText.setText("("+rating+"/5.0)");

            }

        });

        // FONDO ROJO

        final ToggleButton boton_fondo = findViewById(R.id.button_fondo);
        final LinearLayout layoutFondo = findViewById(R.id.layout4);

        boton_fondo.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(boton_fondo.isChecked()){
                    layoutFondo.setBackgroundColor(Color.RED);
                }else{
                    layoutFondo.setBackgroundColor(Color.parseColor("#FAFAFA"));
                }

            }
        });

        // LETRAS ROJAS

        final ToggleButton boton_texto = findViewById(R.id.button_texto);

        boton_texto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(boton_texto.isChecked()){
                    boton_texto.setTextColor(Color.RED);
                }else{
                    boton_texto.setTextColor(Color.BLACK);
                }

            }
        });

        // CHECKBOX PARA MOSTRAR TEXTO

        final CheckBox check_texto = findViewById(R.id.checkbox);
        final TextView texto_check = findViewById(R.id.texto_check);

        check_texto.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(check_texto.isChecked()){
                    texto_check.setVisibility(View.VISIBLE);
                }else{
                    texto_check.setVisibility(View.INVISIBLE);
                }

            }
        });

        // AL HACER UN CLICK LARGO, QUE MUESTRE UN MENSAJE

        final TextView longclick = findViewById(R.id.longclick);

        longclick.setOnLongClickListener(new View.OnLongClickListener() {

            public boolean onLongClick(View v) {
                final Toast toast = Toast.makeText(getApplicationContext(), "¡Muchas gracias!", Toast.LENGTH_LONG);
                toast.show();

                return true;
            }
        });


    }


}
